module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  "plugins": [
    "fp",
    "typescript",
  ],
  "parser": "typescript-eslint-parser",
  "extends": [
    "eslint:recommended",
    "plugin:fp/recommended",
  ],
  "parserOptions": {
    "sourceType": "module"
  },
  "rules": {
    "indent": [
      "error",
      2,
      {
        "SwitchCase": 1
      }
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-undef": "off", // for #416
    "no-unused-vars": "off",
    "no-trailing-spaces": "error",
    "object-shorthand": ["error", "always"],
    "typescript/no-unused-vars": "error",
    "fp/no-nil": "off",
    "fp/no-throw": "off",
  }
};
