/* eslint fp/no-unused-expression: off */
import path from "path";
import fs from "fs";

import {
  Around,
  Cell,
  CellState as state,
  Way,
  Wall,
  Point,
  Directions,
  Direction,
  Ambiguous
} from "./interfaces";
import { parseText } from "../console/util";

export const version = new Date().toString();

export function str2Cell(strs: string[][]): Cell[][] {
  const map = new Map([
    ["x", state.Wall],
    ["X", state.Wall],
    ["s", state.Start],
    ["S", state.Start],
    ["?", state.Ambiguous],
  ]);
  return strs.map(row => row.map(col => ({
    state: map.get(col) || state.Way,
  })));
}

export function expand(question: Cell[][]): Cell[][] {
  return question.reduce((prev: Cell[][], row, rowIndex) => {
    // return prev.concat([row.slice()])
    const vert = Array.from({length: row.length * 2 - 1}).map((_, index) => ({
      state: index % 2 ? state.Wall : state.Ambiguous
    }));
    const expanded = row.map((cell, index) => index === 0 ? [cell] : [{
      state: state.Ambiguous
    }, cell]).reduce((a, b) => a.concat(b), []);
    const toAppend = (rowIndex === 0) ? [expanded] : [vert, expanded];
    return [...prev, ...toAppend];
  }, []);
}

export function getAmbiguouses(question: Cell[][]): Point[]{
  return question.map((row, y) =>
    row.map((cell, x) => cell.state === state.Ambiguous ? x : -1)
      .filter(x => x >= 0).map(x => ({y, x}))
  ).reduce((a,b) => a.concat(b), []);
}

/*
 * questionのスコアを求める。少なければ少ないほどよい
 */
export function score(question: Cell[][]): number {
  const ambiguous = question.reduce(
    (prev, row) => prev + row.filter(cell => cell.state === state.Ambiguous).length, 0
  );
  if(ambiguous > 0){
    return ambiguous;
  }
  return isPassAll(question) ? 0 : Number.POSITIVE_INFINITY;
}
/**
 * Cellの中身を計算する
 */
export function count(map: Cell[][], target: Cell): number {
  return map.reduce(
    (prev, row) => prev + row.filter(cell => cell.state === target.state).length, 0
  );
}

export function solveRuleBase(question: Cell[][]): Cell[][] {
  return question.map((row, y) => row.map((_cell, x) => {
    return {
      state: determine(x, y, question)
    };
  }));
}

/**
 * 残りのAmbiguousを仮定によって解く
 */
export function solveTemporaryPlacement(question: Cell[][]): Cell[][]{
  const ambiguous = getAmbiguouses(question);
  const factory = (c: Cell[][], p: Point, replace: Cell) => c.map((row, y) =>
    row.map((cell, x) => (y === p.y && x === p.x) ? replace : cell)
  );

  const table = ambiguous.map(point => ({
    point,
    cell: determineTemporaryPlacement(question, point)
  })).filter(map => map.cell.state !== state.Ambiguous) ;

  const result = table.reduce((prev, t) => factory(prev, t.point, t.cell), question);
  return result;
}
/**
 * 仮おきによってどちらか判別する
 */
export function determineTemporaryPlacement(map: Cell[][], target: Point): Cell{
  const factory = (c: Cell[][], p: Point, replace: Cell) => c.map((row, y) =>
    row.map((cell, x) => (y === p.y && x === p.x) ? replace : cell)
  );

  const candidate =  [Way, Wall].filter(cell => {
    const tempMap = solve(factory(map, target, cell), 100, 1, true);
    const aroundWay = Directions
      .map(dir => addPoint([target, Direction[dir]]))
      .filter(p => getFromMap(p.x, p.y, tempMap).state === state.Way);
    const searchTarget = aroundWay.reduce((prev: Point[], now) => {
      const duplicated = prev.filter(point => {
        const route = trace(tempMap, [point]);
        return route.filter(p => p.x === now.x && p.y === now.y).length > 0;
      });
      return duplicated.length > 0 ? prev : prev.concat(now);
    }, []).map(p => {
      // return trace(tempMap,[p]).length < wayNum;
      return !isClosed(tempMap,[p]);
    });
    return searchTarget.reduce((a,b) => a && b, true);
  });
  return candidate.length !== 1 ? Ambiguous : candidate[0];
}

/**
 * 周囲の情報を取得する
 * @param original 展開前データの情報を取得する
 */
export function getAround(x: number, y: number, question: Cell[][], original: boolean): Around {
  if(x % 2 + y % 2 > 0 && original){
    throw new Error();
  }
  const d = original ? 2 : 1;
  return {
    north: y - d < 0 ? Wall : question[y - d][x],
    south: y + d >= question.length ? Wall : question[y + d][x],
    west: x - d < 0 ? Wall : question[y][x - d],
    east: x + d >= question[y].length ? Wall : question[y][x + d],
  };
}

export function around2Arr(around: Around): Cell[] {
  const keys:Array<keyof Around> = ["north", "south", "east", "west"];
  return keys.map(key => around[key]);
}

export function determine(x: number, y: number, question: Cell[][]): state {
  const target = question[y][x];
  if(target.state !== state.Ambiguous){
    return target.state;
  }
  const targetAround = getAround(x, y, question, false);
  const around = around2Arr(targetAround);
  const wallLength = around.filter((cell) => cell.state === state.Wall).length;
  const isWall = wallLength >= 3;
  if(isWall) {
    return state.Wall;
  }

  const adjacent: Point[] = Array.from({length: 2}).map((_,index) => {
    return {
      x: x % 2 === 1 ? x - Math.pow(-1, index) : x,
      y: y % 2 === 1 ? y - Math.pow(-1, index) : y,
    };
  });

  const isNotPass = adjacent.map(pos => {
    const isStart = question[pos.y][pos.x].state === state.Start;
    const len = around2Arr(getAround(pos.x, pos.y, question, false)).filter(cell => cell.state === state.Way).length;
    return len === ( isStart ? 1 : 2);
  }).reduce((a, b) => a || b, false);
  if(isNotPass){
    return state.Wall;
  }

  // オリジナルで道であること
  if(isWay(x, y, question)) {
    return state.Way;
  }

  return state.Ambiguous;
}

/**
 * 道の位置が確定しているかを示す.
 * trueなら残りのAmbiguousは道である
 */
export function isConfirmedWay(x: number, y: number, question: Cell[][]): boolean {
  const cell = getFromMap(x,y,question);
  if(cell.state === state.Wall){
    return false;
  }
  const isStart = cell.state === state.Start;
  const aroundWall = around2Arr(getAround(x, y, question, false)).filter(cell => cell.state === state.Wall).length;
  return (aroundWall === 3 || (aroundWall === 2 && !isStart));
}
/**
 * 道であることが確実かどうかを返す
 */
export function isWay(x: number, y: number, map: Cell[][]): boolean {
  const adjacent: Point[] = Directions
    .map(dir => Direction[dir])
    .map(p => ({
      x: x + p.x,
      y: y + p.y,
    }));
  const confirmed = adjacent
    .map(p => isConfirmedWay(p.x, p.y, map));
  const isWayFlag = confirmed
    .reduce((a,b) => (a || b), false);
  return isWayFlag;
}
export function getFromMap(x: number, y:number, map: Cell[][]): Cell {
  if(x < 0 || map[0].length <= x) {
    return Wall;
  }
  if(y < 0 || map.length <= y) {
    return {
      state: state.Wall,
    };
  }
  return map[y][x];
}

export function map2str(target: Cell[][]): string {
  const str = target.map(row =>
    row.map(cell2str).join("")
  ).join("\n");
  return str;
}

export function cell2str(target: Cell): string {
  const map = new Map([
    [state.Wall, "█"],
    [state.Way, "."],
    [state.Ambiguous, String.fromCharCode(0x2591)],
    [state.Start, "S"],
  ]);
  return map.get(target.state) || "*";
}

export function printMap(target: Cell[][]): void {
  process.stdout.write(map2str(target) + "\n");
}

export function getStart(question: Cell[][]): Point {
  const xMap = question.map(row => row.findIndex(cell => cell.state === state.Start));
  const y = xMap.findIndex(x => x >= 0);
  if(y < 0){
    return { x: -1, y: -1 };
  }
  const x = xMap[y];
  return {x, y};
}

/**
 * 決定された終端を返す
 *
 * 上下左右の壁セルが3の道を返す
 */
export function getTerminations(map: Cell[][]): Point[] {
  return Array.from({length: map.length}, (_, i) => i).map(y => {
    return Array.from({length: map[y].length}, (_, i) => i).filter(x => {
      return getFromMap(x,y, map).state === state.Way && around2Arr(getAround(x, y, map, false))
        .filter(cell => cell.state === state.Wall)
        .length === 3;
    }).map(x => {
      return {
        x,
        y,
      };
    });
  }).filter(row => row.length !== 0).reduce((a,b) => a.concat(b), []);
}

export function isPassAll(route: Cell[][]):boolean {
  const numberOfWay = route.reduce((prev, row) => prev + row.filter(cell => cell.state !== state.Wall).length, 0);
  const tracedWay = trace(route, []).length;
  return numberOfWay === tracedWay;
}

export function isSamePoint(a: Point, b: Point): boolean {
  return a.x === b.x && a.y === b.y;
}

export function isClosed(map: Cell[][], route: Point[]):boolean {
  const way = trace(map, route);
  const aroundPoint = way.map(
    p => Directions.map(dir => addPoint([p, Direction[dir]]))
  ).reduce((a, b) => a.concat(b), [])
    .filter(p => !way.find(w => p.x === w.x && p.y === w.y));
  const loophole = aroundPoint.filter(p => getFromMap(p.x, p.y, map).state !== state.Wall);
  return loophole.length === 0;
}

/**
 * Pointを足していく
 */
export function addPoint(points: Point[]): Point {
  return points.reduce((prev, now) => ({
    x: prev.x + now.x,
    y: prev.y + now.y,
  }), {x: 0, y: 0});
}

export function trace(route: Cell[][], path: Point[], allowAmbigous = true): Point[]{
  if(path.length === 0) {
    return trace(route, [getStart(route)], allowAmbigous);
  }
  const current = path[path.length - 1];
  const aroundAll = getAround(current.x, current.y, route, false);
  const wayKey = Directions.filter(key => aroundAll[key].state !== state.Wall && (allowAmbigous || aroundAll[key].state !== state.Ambiguous ));
  const way = wayKey.map(key => addPoint([current, Direction[key]]));
  const notPassed = way.filter(point => path.findIndex(p => isSamePoint(p, point))< 0);
  if(notPassed.length < 1){
    return path;
  }
  const next = notPassed[0];
  return trace(route, path.concat(next),allowAmbigous);
}

/**
 * 問題を解く
 *
 * @param question 問題
 * @param retry 再試行回数
 * @param stage 今何回目かの記録
 * @param onlyRuleBase ルールベースのみで解くか
 * @return 回答
 */
export function solve(question: Cell[][], retry = 100, stage = 1, onlyRuleBase = false): Cell[][] {
  if(retry < 1){
    return question;
  }
  const currentScore = score(question);
  if(currentScore === 0) {
    return question;
  }
  const simpleSolve = solveRuleBase(question);

  if(currentScore - score(simpleSolve) > 0){
    return solve(simpleSolve, retry - 1, stage + 1, onlyRuleBase);
  }
  if(onlyRuleBase){
    return simpleSolve;
  }
  const tempPlaceSolve = solveTemporaryPlacement(simpleSolve);
  return currentScore - score(tempPlaceSolve) > 0
    ? solve(tempPlaceSolve, retry - 1, stage + 1, onlyRuleBase)
    : tempPlaceSolve;
}

export function main(args: string[]) {
  if(args.length < 1){
    process.stderr.write(`${process.argv[1]} mapFile\n`);
    return;
  }
  const f = path.join(process.cwd(), args[0]);
  const content = fs.readFileSync(f, "utf8");
  const question = parseText(content);
  const tries = 30;
  printMap(question);
  const stage0 = expand(question);
  printMap(stage0);
  const answer = solve(stage0, tries);
  const answerScore = score(answer);

  process.stdout.write("answer\n");
  printMap(answer);
  process.stdout.write(`\nanswer score is ${answerScore}\n`);
  return answer;
}

export default {
  map2str,
  expand,
  score,
  solveRuleBase,
  determine,
  version,
  isConfirmedWay,
  isWay,
  getTerminations,
  solve,
  trace,
  getStart,
  isClosed,
  determineTemporaryPlacement,
  cell2str,
  getAmbiguouses,
  state
};

if(require.main === module ){
  main(process.argv.slice(2));
}
