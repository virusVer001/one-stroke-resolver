export enum CellState {
  Wall = 1,
    Way,
    Ambiguous,
    Start,
}

export interface Cell {
  state: CellState;
}

export interface Point {
  x: number;
  y: number;
}

export interface Around {
  north: Cell;
  south: Cell;
  east: Cell;
  west: Cell;
}

export const Wall: Cell = Object.freeze({
  state: CellState.Wall,
});

export const Way: Cell = Object.freeze({
  state: CellState.Way,
});

export const Ambiguous: Cell = Object.freeze({
  state: CellState.Ambiguous,
});

export const Start: Cell = Object.freeze({
  state: CellState.Start,
});

export const Direction: { [key in keyof Around]: Point} = {
  north: {
    x: 0,
    y: -1,
  },
  south: {
    x: 0,
    y: 1,
  },
  east: {
    x: 1,
    y: 0,
  },
  west: {
    x: -1,
    y: 0,
  }
};

export const Directions: Array<keyof Around> = ["north", "south", "east", "west"];
