import { remote } from "electron";
import { h, app, ActionsType } from "hyperapp";
import devtools from "hyperapp-redux-devtools";

import core from "../core/index";
import { parseText, CellStateStr } from "../console/util"
import { Cell, Point } from "../core/interfaces";

import * as fs from "fs";
import path from "path";

interface State {
  map: Cell[][];
  highlights: Point[];
  files: string[];
  result: string;
  selecting: Point | undefined;
  filename: string;
}

interface Actions {
  setFiles(files: string[]): State;
  loadFile(file: string): State;
  setMap({filename, map}: {filename: string, map: Cell[][]}): State;
  step(): State;
  getTermination(): State;
  selectCell({x, y}: Point): State;
  solveRuleBase(): State;
  solve(): State;
  trace(allowAmbious: boolean): State;
  determineWithTempPlace(): State;
  solveAllAmbigous(): State;
  reloadFiles(): State;
}

const problems = path.join(__dirname, "..", "..", "..", "data");

const state: State = {
  map: [],
  files: [],
  result: "",
  highlights: [],
  selecting: undefined,
  filename: ""
};

const actions: ActionsType<State, Actions> = {
  setFiles: (files: string[]) => (state): State => ({
    ...state,
    files: files,
  }),
  setMap: ({filename, map}: {filename: string, map: Cell[][]}) => (state): State => ({
    ...state,
    filename,
    highlights: [],
    map,
  }),
  loadFile: (file: string) => (_state, actions) => {
    fs.readFile(file, 'utf8', (err, data) => {
      if(err){
        window.alert(err);
      }
      actions.setMap({filename: file,map: core.expand(parseText(data))});
    });
  },
  step: () => (state) => ({
    ...state,
    map: core.solveRuleBase(state.map)
  }),
  getTermination: () => (state) => {
    const result = core.getTerminations(state.map);
    return {
      ...state,
      result: result.map(p => `(${p.x}, ${p.y})`).join("\n"),
      highlights: result,
    };
  },
  selectCell: ({x, y}) => (state: State) => ({
      ...state,
      selecting: (state.selecting !== undefined && x === state.selecting.x && y === state.selecting.y) ? undefined : {x,y},
  }),
  solveRuleBase: () => (state) => {
    const prevScore = core.score(state.map);
    function f(s: State, prevScore: number): State{
      const newMap = core.solveRuleBase(s.map);
      const score = core.score(newMap);
      const diffScore = score - prevScore;
      return diffScore < 0 ? f({...s, map: newMap}, score) : s;
    }
    return f(state, prevScore);
  },
  trace: (allowFlag: boolean) => (state) => ({
    ...state,
    highlights: core.trace(state.map, state.selecting ? [state.selecting] : [], allowFlag)
  }),
  determineWithTempPlace: () => (state) => ({
    ...state,
    result: state.selecting ? CellStateStr[core.determineTemporaryPlacement(state.map, state.selecting).state] : ""
  }),
  solveAllAmbigous: () => (state: State) => {
    const ambigououses = core.getAmbiguouses(state.map);
    const table = ambigououses.map(point => ({
      point,
      cell: core.determineTemporaryPlacement(state.map, point)
    })).filter(map => map.cell.state !== core.state.Ambiguous) ;
    return {
      ...state,
      map: state.map.map(
        (line, y) => line.map(
          (cell, x) => {
            const data = table.find(data => data.point.x === x && data.point.y === y);
            return data ? data.cell : cell;
          }
        )
      )
    };
  },
  solve: () => (state: State) => ({
    ...state,
    map: core.solve(state.map, 100, 1, false)
  }),
  reloadFiles: () => (_state: State, actions) => {
    fs.readdir(problems, (err, list) => {
      if(err){
        console.error(err);
        return;
      }
      actions.setFiles(list);
    });
  }
};

const view = (state: State, actions: Actions) => (
  <div>
    <div>onestroke resolver</div>
    <div>{state.filename}</div>
    <table class="map">
      {
        state.map.map((line,y) => (
          <tr>
            {
              line.map((cell, x) => (
                <td class={CellStateStr[cell.state]} onclick={() => actions.selectCell({x ,y})}>
                  <div class={(state.highlights.find(p => (p.x === x && p.y === y)) ? "highlight-cell" : "")}>
                    { (state.selecting && state.selecting.x === x && state.selecting.y === y) && <div class="selected"></div>}
                  </div>
                </td>
              ))
            }
          </tr>
        ))
      }
    </table>
    <button onclick={() => actions.step()}>step(rulebase)</button>
    <button onclick={() => actions.solveRuleBase()}>solve rulebase</button>
    <button onclick={() => actions.getTermination()}>getTermination</button>
    <button onclick={() => actions.trace(true)}>trace</button>
    <button onclick={() => actions.trace(false)}>trace not allow ambigouous</button>
    <button onclick={() => actions.determineWithTempPlace()}>determine with temp place</button>
    <button onclick={() => actions.solveAllAmbigous()}>solveAllAmbigous</button>
    <button onclick={() => actions.solve()}>solve</button>
    <button onclick={() => actions.reloadFiles()}>Reload Files</button>
    <p>{problems}</p>
    <p style={{whiteSpace: "pre-line"}}>{state.result}</p>
    <details>
      <summary>
        ファイル一覧
      </summary>
    <ul>
      {
        state.files.map(file => (<li><button onclick={() => actions.loadFile(path.join(problems, file))}>{file}</button></li>))
      }
    </ul>
    </details>
  </div>
);

const main = devtools(app)(state,actions,view,document.getElementById("app"));
console.log(remote.require("fs"));
main.reloadFiles();

export default main;
