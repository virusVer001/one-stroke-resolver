import { Cell } from "../core/interfaces";

export type Map = Cell[][];
export interface CLIState {
  cwd: string;
  current: Map;
  history: Map[];
}

export interface Command {
  command: string;
  help: string;
  handler: (state: CLIState, args: string[]) => Promise<CLIState>;
  completer?: (state: CLIState, line: string) => Promise<[string[], string]>;
}
