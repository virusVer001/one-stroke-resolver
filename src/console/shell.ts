/* eslint fp/no-unused-expression: off */
import readline from "readline";

import { Command, CLIState } from "./interfaces";

function generateIndexMap(commands: Command[]) {
  const cmdMapArr = commands.map((cmd, index): [string, number] => [cmd.command, index]);
  return new Map(cmdMapArr);
}

export function line(rl: readline.ReadLine, state: CLIState, commands: Command[], line: string): Promise<CLIState>{
  const args = line.trim().split(" ").filter(v => v);
  const command = args[0];
  const commandIndex = generateIndexMap(commands);

  if(command === "quit" || command === "exit"){
    rl.close();
    return Promise.resolve(state);
  }

  const index = commandIndex.get(command);
  const cmd = index !== undefined ? commands[index] : undefined;

  return Promise.resolve(cmd).then(cmd => {
    if(cmd === undefined) {
      process.stdout.write(`missing command ${args[0]}\n`);
      return state;
    }
    return cmd.handler(state, args.slice(1));
  });
}

export function completer(state: CLIState, commands: Command[], line: string) {
  const args = line.split(" ");
  const cmd = args[0];
  const commandIndex = generateIndexMap(commands);
  const index = commandIndex.get(cmd);
  if(index !== undefined) {
    const cmdObj = commands[index];
    if(cmdObj.completer !== undefined){
      return cmdObj.completer(state, args.slice(1).join(" "));
    }
  }
  const keys: string[] = commands.map(c => c.command).concat("quit", "exit");
  const hits = keys.filter((c) => c.startsWith(line));
  const result: [string[], string] = [hits.length ? hits : keys, line];
  return Promise.resolve(result);
}

export default {
  completer,
  line,
};
