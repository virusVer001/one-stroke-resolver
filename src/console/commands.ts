/* eslint fp/no-unused-expression: off */
import fs from "fs";
import chalk from "chalk";
import path from "path";
import util from "util";
import coreObj from "../core/index";

import { Wall, Way } from "../core/interfaces";

import { parseText, CellStateStr } from "./util";
import { Command, CLIState, Map } from "./interfaces";

const stat = util.promisify(fs.stat);
/* eslint-disable-next-line fp/no-let */
let core: typeof coreObj = coreObj;

export const generateCLI = () => {
  // eslint-disable-next-line fp/no-delete
  delete require.cache[require.resolve("../core/index")];
  // eslint-disable-next-line fp/no-mutation
  core = require("../core/index");
  const version = "0.0.3";
  const commands: Command[] = [{
    command: "help",
    help: "show help messages",
    async handler(state){
      const len = Math.max(...commands.map(cmd => cmd.command.length));
      process.stdout.write(commands.map(cmd => `${cmd.command.padEnd(len+1," ")}: ${cmd.help}`).join("\n")+"\n");
      process.stdout.write("quit\t\t: quit program\n");
      return state;
    }
  }, {
    command: "load",
    help: "load cell from file",
    async handler(state: CLIState, args: string[]){
      const filename = path.join(state.cwd, args[0]);
      return util.promisify(fs.readFile)(filename, { encoding: "utf8"}).then(str => {
        const map = parseText(str);
        process.stdout.write(core.map2str(map)+"\n");
        return Object.assign({}, state, {
          current: map,
        });
      }).catch(e => {
        process.stdout.write(chalk.red(`Error occured: ${e.toString()}\n`));
        return state;
      });
    },
    completer(state, line){
      return new Promise((resolve, reject)=> {
        fs.readdir(state.cwd, (err, list) => {
          if(err){
            reject(err);
          }
          const hits = list.filter((c) => c.startsWith(line));
          resolve([hits.length ? hits : list, line]);
        });
      });
    }
  }, {
    command: "list-file",
    help: "list files",
    async handler(state){
      process.stdout.write(fs.readdirSync(state.cwd).join("\n")+"\n");
      return state;
    }
  }, {
    command: "version",
    help: "show version",
    async handler(state){
      process.stdout.write(
        `version: ${version}\n`
        + `core: ${core.version}\n`
      );
      return state;
    }
  }, {
    command: "cd",
    help: "change directory",
    async handler(state, args){
      if(args.length < 1){
        process.stdout.write("cd <PATH>\n");
        return state;
      }
      const dir = path.join(state.cwd, args[0]);
      return stat(dir).then((stat) => {
        if(!stat.isDirectory()){
          process.stdout.write(chalk.red(`${args[0]} is not directory\n`));
        }
        return stat.isDirectory() ? Object.assign({}, state, { cwd: dir}) : state;
      }).catch((err) => {
        process.stdout.write(err.toString()+"\n");
        return state;
      }).then(state => {
        process.stdout.write(`${state.cwd}\n`);
        return state;
      });
    },
    completer(state, line) {
      return new Promise<string[]>((res, rej) => {
        fs.readdir(state.cwd, (err, list) => {
          err ? rej(err) : res(list);
        });
      }).then(list => {
        return Promise.all(
          list.map((f => stat(path.join(state.cwd, f))
            .then((stat):[string, boolean] => [f, stat.isDirectory()])
            .catch((err):[string, boolean] => {
              process.stdout.write(chalk.red(`Error occured: ${err.toString()}\n`));
              return [f, false];
            })
          ))
        ).then((results): [string[], string] => {
          const dirs = results.filter(l => l[1]).map(l => l[0]);
          const hits = dirs.filter((c) => c.startsWith(line));
          return [hits.length > 0? hits : list, line];
        });
      });
    },
  }, {
    command: "pwd",
    help: "print working directory",
    async handler(state){
      process.stdout.write(`${state.cwd}\n`);
      return state;
    }
  }, {
    command: "print",
    help: "print now map",
    async handler(state){
      const mapStr = core.map2str(state.current);
      const xAxis = "\\_"+Array.from({length: state.current[0].length}, (_, i) => i % 10).join("");
      const addedYAxis = mapStr.split("\n").map((line, y) => `${y % 10}|`+line).join("\n");
      process.stdout.write(`${xAxis}\n${addedYAxis}\n`);
      return state;
    }
  }, {
    command: "dump",
    help: "dump state",
    async handler(state){
      process.stdout.write(
        `cwd: ${state.cwd}\nmap:\n` + core.map2str(state.current) + "\n"
      );
      return state;
    }
  }, {
    command: "expand",
    help: "expand map",
    async handler(state){
      const newMap = core.expand(state.current);
      process.stdout.write(`${core.map2str(newMap)}\n`);
      return updateCurrent(state, newMap);
    }
  }, {
    command: "undo-pop",
    help: `pop from history ${chalk.yellow("WARN: remove current state")}`,
    async handler(state){
      if(state.history.length < 1){
        process.stdout.write(chalk.red("history is empty\n"));
      }
      const newState = undoMap(state);
      process.stdout.write(`${core.map2str(newState.current)}\n`);
      return newState;
    }
  }, {
    command: "show-last",
    help: "show last map",
    async handler(state){
      if(state.history.length < 1){
        process.stdout.write(chalk.red("history is empty\n"));
      } else {
        process.stdout.write(core.map2str(state.history[0])+"\n");
      }
      return state;
    }
  }, {
    command: "solve",
    help: `<rulebaseOnly=true>: solve current map ${chalk.yellow("rulebaseOnly set true force")}`,
    async handler(state){
      const prevScore = core.score(state.current);
      const map = core.solve(state.current, 100, 1, true);
      const newScore = core.score(map);
      const diff = newScore - prevScore;
      process.stdout.write(core.map2str(map) + `\n\nprevScore: ${prevScore}\nnew Score: ${newScore}\ndiff     : ${diff}\n`);
      if(diff === 0){
        process.stdout.write(chalk.yellow("nothing changed.\n"));
      }
      return Math.abs(diff) >0 ? updateCurrent(state, map) : state;
    }
  }, {
    command: "rulebase",
    help: "solve with rulebase",
    async handler(state) {
      const prevScore = core.score(state.current);
      const map = core.solveRuleBase(state.current);
      const newScore = core.score(map);
      const diff = newScore - prevScore;
      process.stdout.write(core.map2str(map) + `\n\nprevScore: ${prevScore}\nnew Score: ${newScore}\ndiff     : ${diff}\n`);
      if(diff === 0){
        process.stdout.write(chalk.yellow("nothing changed.\n"));
      }
      return Math.abs(diff) >0 ? updateCurrent(state, map) : state;
    }
  }, {
    command: "determine",
    help: "determine <x> <y> with rule base",
    async handler(state, args) {
      const x = parseInt(args[0], 10);
      const y = parseInt(args[1], 10);
      if(Number.isNaN(x) || Number.isNaN(y)){
        process.stdout.write("determine <x> <y> with rule base\n");
        return state;
      }
      if(y >= state.current.length){
        process.stdout.write(chalk.red(`out of index[y] ${y} >= ${state.current.length}`));
        return state;
      }
      if(x >= state.current[y].length){
        process.stdout.write(chalk.red(`out of index[x] ${x} >= ${state.current[x].length}`));
        return state;
      }
      const result = core.determine(x, y, state.current);
      process.stdout.write(`result: ${CellStateStr[result]}\n`);
      return state;
    }
  }, {
    command: "confirmed",
    help: "<x> <y> return around way is confirmed",
    async handler(state, args) {
      if(args.length < 2){
        process.stdout.write(chalk.red("arguments required 3\n"));
        return state;
      }
      const x = parseInt(args[0], 10);
      const y = parseInt(args[1], 10);
      if(Number.isNaN(x) || Number.isNaN(y)){
        process.stdout.write(chalk.red("x or y is not number\n"));
        return state;
      }
      const result = core.isConfirmedWay(x, y, state.current);
      process.stdout.write(`${result}\n`);
      return state;
    },
  }, {
    command: "temp-place",
    help: "<x> <y> <way=false>: set <x> <y> to (<way> ? way : wall)",
    async handler(state, args){
      if(args.length < 2){
        process.stdout.write(chalk.red("arguments required 3\n"));
        return state;
      }
      const x = parseInt(args[0], 10);
      const y = parseInt(args[1], 10);
      const wayFlag = args[2] ? args[2].trim() !== "false" : false;
      if(Number.isNaN(x) || Number.isNaN(y)){
        process.stdout.write(chalk.red("x or y is not number\n"));
        return state;
      }
      const replaceTo = wayFlag ? Way : Wall;
      const newMap = state.current.map((row, cellY) =>
        row.map((cell, cellX) => (y === cellY && x === cellX) ? replaceTo : cell)
      );
      return updateCurrent(state, newMap);
    }
  }, {
    command: "score",
    help: "calculate score",
    async handler(state){
      process.stdout.write(`score: ${core.score(state.current)}\n`);
      return state;
    }
  }, {
    command: "trace",
    help: "<x> <y>: trace path from <x> <y> or start",
    async handler(state,args){
      if(args.length !== 0 && args.length !== 2){
        process.stdout.write(chalk.red("arguments number must be 0 or 2\n"));
        return state;
      }
      const point = args.length === 0 ? core.getStart(state.current) : {
        x: parseInt(args[0], 10),
        y: parseInt(args[1], 10),
      };
      const route = core.trace(state.current, [point]);
      process.stdout.write(route.map(pos => `(${pos.x},${pos.y})`).join("\n")+"\n");
      return state;
    }
  }, {
    command: "is-closed",
    help: "<x> <y>: print which closed path",
    async handler(state, args){
      if(args.length !== 0 && args.length !== 2){
        process.stdout.write(chalk.red("arguments number must be 0 or 2\n"));
        return state;
      }
      const point = args.length === 0 ? core.getStart(state.current) : {
        x: parseInt(args[0], 10),
        y: parseInt(args[1], 10),
      };
      const result = core.isClosed(state.current, [point]);
      process.stdout.write(`(${point.x}, ${point.y}): ${result}\n`);
      return state;
    }
  }, {
    command: "determ-temp",
    help: "<x> <y>: determine by temp place",
    async handler(state, args){
      if(args.length !== 2){
        process.stdout.write(chalk.red("arguments number must be 2\n"));
        return state;
      }
      const target = {
        x: parseInt(args[0], 10),
        y: parseInt(args[1], 10),
      };
      const result = core.determineTemporaryPlacement(state.current, target);
      process.stdout.write(`(${target.x}, ${target.y}) => ${CellStateStr[result.state]}\n`);
      return state;
    }
  }, {
    command: "error",
    help: "throw error",
    async handler() {
      throw new Error("error occured");
    }
  }];

  return {
    version,
    commands,
  };
};

function updateCurrent(state: CLIState, map: Map): CLIState{
  return Object.assign({}, state, {
    current: map,
    history: [state.current, ...state.history],
  });
}

function undoMap(state: CLIState): CLIState {
  if(state.history.length < 1){
    return state;
  }
  return Object.assign({}, state, {
    current: state.history[0],
    history: state.history.slice(1)
  });
}
