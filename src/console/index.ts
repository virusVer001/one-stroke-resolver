/* eslint fp/no-unused-expression: off */
import readline from "readline";

import { generateCLI } from "./commands";
import { CLIState, Command } from "./interfaces";
import shellObj from "./shell";

function initCLIState(): CLIState {
  return {
    cwd: process.cwd(),
    current: [],
    history: [],
  };
}

function main(initState: CLIState) {
  /* eslint-disable fp/no-let */
  let state = initState;
  let shell = shellObj;
  let cli = generateCLI();
  /* eslint-enable */

  const completer: readline.AsyncCompleter = (line, callback) => {
    shell.completer(state, cli.commands, line).then(result => {
      callback(null, result);
    }).catch(err => {
      callback(err, [[], line]);
    });
  };

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    completer,
  });

  process.stdout.write("CTRL-U to delete line\n");

  const metaCmd: Command[] = [{
    command: "reload",
    help: "reload module",
    async handler(state){
      /* eslint-disable fp/no-delete */
      delete require.cache[require.resolve("./commands")];
      delete require.cache[require.resolve("./util")];
      /* eslint-enable */
      const newCLI = require("./commands");
      // eslint-disable-next-line fp/no-mutation
      cli = newCLI.generateCLI();
      process.stdout.write("reloaded\n");
      process.stdout.write(`${cli.version}\n`);
      return state;
    }
  }];

  const getCommands = () => cli.commands.concat(metaCmd);

  rl.setPrompt("Solver> ");
  rl.prompt();

  rl.on("line", (line: string) => {
    rl.pause();
    Promise.resolve(state)
      .then(state => shell.line(rl, state, getCommands(), line)).catch(e => {
        // .then(state => lineStub(rl, state, getCommands(), line)).catch(e => {
        console.log("error occured");
        // throw e;
        console.log(e);
        return state;
      })
      .then(newState => {
        // eslint-disable-next-line fp/no-mutation
        state = newState;
        rl.prompt();
      });
  });
  rl.on("close", () => {
    console.log("closed");
    process.stdout.write("bye\n");
    process.exit(0);
  });
  rl.on("SIGINT", () => {
    process.exit(1);
  });
}

export async function lineStub(_rl: any, state: CLIState, _commands: any, line: string){
  if(line.trim() === "error"){
    throw new Error("error command");
  }
  process.stdout.write(`executed ${line}\n`);
  return state;
}

if(require.main === module ){
  const state = initCLIState();
  main(state);
}
