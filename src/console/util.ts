import { Cell, CellState } from "../core/interfaces";
import { str2Cell } from "../core/index";

export function parseText(file: string): Cell[][] {
  const lines = file.split("\n").filter(v => v.length > 0);
  const flagSyntax = /^\s*(\S+)\s*=\s*(\S+)\s*$/;

  const mapStr = lines.filter(line => !flagSyntax.test(line)).map(line => line.split(""));
  return str2Cell(mapStr);
}

export const CellStateStr = {
  [CellState.Wall]: "Wall",
  [CellState.Ambiguous]: "Ambiguous",
  [CellState.Way]: "Way",
  [CellState.Start]: "Start",
};
