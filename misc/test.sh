#!/bin/sh
# find data/ -print0 | xargs -0 -n1 node -r source-map-support/register dist/src/core/index.js

for i in data/*
do
  tput reset
  echo "tring $i";
  if ! node -r source-map-support/register dist/src/core/index.js "$i"; then
    exit 1;
  fi
  sleep 1
done
