/* eslint fp/no-unused-expression: off */
import "mocha";

import {
  str2Cell,
  score,
  getStart,
  isPassAll,
  trace,
  getAmbiguouses,
  isConfirmedWay,
  isWay,
  getTerminations,
} from "../src/core/index";
import { Start, Way, Wall, Ambiguous, Point } from "../src/core/interfaces";
import { assertCellMap } from "./helper";
import assert from "power-assert";

describe("str2Cell", () => {
  it("should return correct result", () => {
    const expected = [
      [Wall, Ambiguous],
      [Way, Start],
    ];
    const actual = str2Cell([
      "x?",
      ".s",
    ].map(row => row.split("")));
    assertCellMap(expected, actual);
  });
});

describe("score", () => {
  it("should return 0 when passed map which solved", () => {
    const map = [
      [ Way, Way, Way],
      [ Way, Wall, Way],
      [ Start, Way, Way],
    ];
    const expected = 0;
    const actual = score(map);
    assert(expected === actual);
  });
  it("should return number of ambiguous when it exists", () => {
    const map = [
      [ Way, Ambiguous, Way],
      [ Ambiguous, Wall, Ambiguous],
      [ Start, Ambiguous, Way],
    ];
    const expected = 4;
    const actual = score(map);
    assert(expected === actual);
  });
  it("should return more than 0 if closed loop detected", () => {
    const map = [
      [ Way, Way, Way, Wall, Way, Way, Way],
      [ Way, Wall, Way, Wall, Way, Wall, Way],
      [ Start, Way, Way, Wall, Way, Way, Way],
    ];
    const actual = score(map);
    assert(actual > 0);
  });
});

describe("getStart", () => {
  it("should return start point", () => {
    const map = [
      [Wall, Way, Way],
      [Way, Ambiguous, Way],
      [Way, Way, Start],
    ];
    const actual = getStart(map);
    assert(actual.x === 2);
    assert(actual.y === 2);
  });
});

describe("isPassAll", () => {
  it("should return true when it pass all", () => {
    const map = [
      [Start, Way, Way],
      [Way, Wall, Way],
      [Way, Way, Way],
    ];
    assert(isPassAll(map));
  });
  it("should return false when it doesn't pass all", () => {
    const map = [
      [Start, Wall, Way],
      [Way, Wall, Wall],
      [Way, Way, Way],
    ];
    assert(!isPassAll(map));
  });
});

describe("trace", () => {
  it("should pass all way when it is possible", () => {
    const map = [
      [Start, Way],
      [Way, Ambiguous],
    ];
    const path: Point[] = [];
    assert(trace(map, path).length === 4);
  });
  it("should not path closed way", () => {
    const map = [
      [Start, Wall, Way],
      [Ambiguous, Wall, Wall],
      [Way, Ambiguous, Way],
    ];
    const path: Point[] = [];
    const result = trace(map, path);
    assert(result.length === 5);
    assert(result.findIndex(p => p.x === 2 && p.y === 0) < 0);
  });

  it("should not path ambiguous when ambiguous is not allowed", ()=> {
    const map = [
      [Start, Wall, Way],
      [Way, Wall, Ambiguous],
      [Way, Ambiguous, Way],
    ];
    const path: Point[] = [];
    debugger;
    const result = trace(map, path, false);
    assert(result.length === 3);
  });
});

describe("getAmbiguouses", () => {
  it("should return all amiguouses point", () => {
    const map = [
      [Wall, Ambiguous, Way],
      [Ambiguous, Ambiguous, Ambiguous],
      [Way, Ambiguous, Start],
    ];
    const actual = getAmbiguouses(map);
    const expected = [
      {x: 1, y: 0},
      {x: 0, y: 1},
      {x: 1, y: 1},
      {x: 2, y: 1},
      {x: 1, y: 2},
    ];
    const diff = actual.filter(
      a => !expected.find(b => (a.x === b.x && a.y === b.y))
    ).concat(
      expected.filter(a => !expected.find(b => (a.x === b.x && a.y === b.y)))
    );
    assert(diff.length === 0);
  });
});

describe("isConfirmedWay", () => {
  describe("when target is not start", () => {
    it("should return true when around wall number is 2", () => {
      const map = [
        [Wall, Wall, Wall],
        [Ambiguous, Way, Wall],
        [Way, Ambiguous, Wall],
      ];
      const confirmed = isConfirmedWay(1, 1, map);
      assert(confirmed);
    });
    it("should return false when around wall number is 1", () => {
      const map = [
        [Wall, Wall, Wall],
        [Ambiguous, Way, Ambiguous],
        [Way, Ambiguous, Wall],
      ];
      const confirmed = isConfirmedWay(1, 1, map);
      assert(!confirmed);
    });
    it("should return true when around wall number is 3", () => {
      const map = [
        [Wall, Wall, Wall],
        [Wall, Way, Wall],
        [Way, Ambiguous, Wall],
      ];
      const confirmed = isConfirmedWay(1, 1, map);
      assert(confirmed);
    });
  });
  describe("when target is start", () => {
    it("should return false when around wall number is 2", () => {
      const map = [
        [Wall, Wall, Wall],
        [Ambiguous, Start, Wall],
        [Way, Ambiguous, Wall],
      ];
      const confirmed = isConfirmedWay(1, 1, map);
      assert(!confirmed);
    });
    it("should return true when around wall number is 3", () => {
      const map = [
        [Wall, Wall, Wall],
        [Wall, Start, Wall],
        [Way, Ambiguous, Wall],
      ];
      const confirmed = isConfirmedWay(1, 1, map);
      assert(confirmed);
    });
  });
});

describe("isWay", () => {
  describe("not around of start", () => {
    it("should return true when expected pattern 1", () => {
      const map = [
        [Way, Ambiguous, Way, Ambiguous, Way],
        [Ambiguous, Wall, Ambiguous, Wall, Ambiguous],
        [Way, Wall, Wall, Wall, Way],
      ];
      assert(isWay(1, 0, map), "(1,0) must be way");
      assert(isWay(3, 0, map), "(3,0) must be way");
      assert(isWay(4, 1, map), "(4,1) must be way");
    });
    it("should return false when expected pattern 1", () => {
      const map = [
        [Way, Ambiguous, Way, Ambiguous, Way],
        [Ambiguous, Wall, Ambiguous, Wall, Ambiguous],
        [Way, Wall, Wall, Wall, Way],
      ];
      assert(!isWay(2, 1, map), "(2,1) must not be way");
    });
    it("should return false when expected pattern 2", () => {
      const map = [
        [Wall, Ambiguous,Wall, Ambiguous,Wall],
        [Ambiguous, Way, Ambiguous, Way, Ambiguous],
        [Wall, Ambiguous,Wall, Ambiguous,Wall],
      ];
      assert(!isWay(2, 1, map), "(2,1) ust not be way");
      assert(!isWay(4, 1, map), "(4,1) ust not be way");
    });
  });
});

describe("getTerminations", () => {
  it("should return correct end term", () => {
    const map = [
      [Way, Wall, Way],
      [Ambiguous, Wall, Way],
      [Way, Wall, Way],
    ];
    const actual = getTerminations(map);
    assert(actual.length === 4);
    [[0,0],[2,0],[2,0],[2,2]].forEach(point => {
      assert(actual.find(p => (p.x === point[0] && p.y === point[1])), `(${point[0]},${point[1]}) is termination`);
    });
  });
  it("should return empty array when terminations is not exist", () => {
    const map = [
      [Way, Way, Way],
      [Ambiguous, Wall, Way],
      [Way, Way, Way],
    ];
    const actual = getTerminations(map);
    assert(actual.length === 0);
  });
  it("should return only way", () => {
    const map =str2Cell([
      "XXXXX",
      "XX XX",
      "XX XX",
      "XX XX",
      "XXXXX"
    ].map(r => r.split("")));
    const actual = getTerminations(map);
    assert(actual.length === 2);
  });
});
