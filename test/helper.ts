/* eslint fp/no-unused-expression: off */

import { Cell, Wall, Way, Ambiguous, Start } from "../src/core/interfaces";
import assert = require("power-assert");

export function assertCellMap(expected: Cell[][], actual: Cell[][]):void {
  expected.forEach((row, y) => {
    row.forEach((expectedCell, x) => {
      assert(expectedCell.state === actual[y][x].state, `[${y}][${x}] is difference`);
    });
  });
}

describe("assertCellMap", () => {
  it("should pass when expected and actual are both cell", () => {
    const generate: () => Cell[][] = () => ([
      [Wall, Way],
      [Start, Ambiguous],
    ]);
    const expected = generate();
    const actual = generate();
    assert.doesNotThrow(() => {
      assertCellMap(expected, actual);
    });
  });
  it("should throw when expected and actual are different", () => {
    const expected = [
      [Wall, Way],
      [Start, Ambiguous],
    ];
    const actual = [
      [Wall, Wall],
      [Start, Ambiguous],
    ];
    assert.throws(() => {
      assertCellMap(expected, actual);
    }, assert.AssertionError);
  });
});
